import React, { useState, useEffect } from 'react'
import { makeStyles, Tooltip, Hidden } from '@material-ui/core';
import { format, getDay, getMonth, isSameDay } from 'date-fns';
import clsx from 'clsx'
import DateDescription from '../types/DateDescription';

interface DaysProps {
    daysInMonth: Date[];
    selectedDate: Date;
    dateWithDescription: DateDescription[];
    onClick: (date: Date) => void;
}

const useStyles = makeStyles(theme => ({
  weekdayContainer: {
    padding: 0,
    margin: 0,
    [theme.breakpoints.down('sm')]: {
      display: 'inline-block',
      width: '100%',
      textAlign: 'left',
    },
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      flex: 1,
      flexWrap: 'wrap',
      justifyContent: 'flex-start',
    },
  },
  day: {
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      textAlign: 'left',
      borderLeft: '5px solid #89cff0',
      borderBottom: '3px solid #89cff0',
      paddingLeft: theme.spacing(4),
    },
    [theme.breakpoints.up('sm')]: {
      alignItems: 'center',
      display: 'flex',
      flexBasis: '14.2%',
      border: 'none',
      padding: 0,
    },
    height: '100px',
    fontSize: 16,
    fontWeight: 'bold',
    listStyle: 'none',
    position: 'relative',
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
  },
  blank: {
    display: 'flex',
    flexBasis: '14.2%',
  },
  mark: {
    [theme.breakpoints.up('sm')]: {
      '&::before': {
        content: '""',
        width: 0,
        height: 0,
        borderTop: '10px solid red',
        borderRight: '10px solid transparent',
        position: 'absolute',
        top: 0,
        left: 0
      }
  }
  },
  date: {
    [theme.breakpoints.down('sm')]: {
      width: theme.spacing(9),
    },
    [theme.breakpoints.up('sm')]: {
      textAlign: 'center',
      flex: 1,
    }
  },
  selectedDate: {
    [theme.breakpoints.up('sm')]: {
      borderRadius: '50%'
    },
    backgroundColor: '#89cff0',
  },
  tooltip: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    fontSize: '18px'
  }
}));

const Days: React.FC<DaysProps> = (props) => {
  const {daysInMonth, selectedDate, onClick, dateWithDescription} = props;
  const classes = useStyles(props);
  const [totalDays, setTotalDays] = useState<string[]>([]);
  const currentMonth = getMonth(daysInMonth[0]);
  const isSelectedMonth: boolean = getMonth(selectedDate) === getMonth(daysInMonth[0]);

  useEffect(() => {
      const days = [];
      var firstDay: number = getDay(daysInMonth[0]);
      if (firstDay === 0) {
        firstDay = 6;
      } else {
        firstDay -= 1;
      }
      for (let i = 0; i < firstDay; i++) {
        days.push('');
      }
      for (let day of daysInMonth) {
        days.push(format(day, 'd'));
      }
      setTotalDays(days);
    }, [daysInMonth]
  )

  const isMarkDate = (date: Date): [boolean, string] =>  {
    const index = dateWithDescription.findIndex(day => isSameDay(day.date, date));
    if (index !== -1) {
      return [true, dateWithDescription[index].description]
    }
    return [false, ''];
  }

  return (
    <div>
        <ul className={classes.weekdayContainer}>
          {totalDays.map((day, index) => {
            const isSelected = isSelectedMonth && day === format(selectedDate, 'd');
            const date: Date = new Date(2020, currentMonth, parseInt(day));
            const [isMark, withDescription]: [boolean, string] = isMarkDate(date);
            if (day === '') {
              return <li key={index} className={classes.blank}></li>
            } else {
                return <Tooltip key={index} classes= {{tooltip: classes.tooltip}} title={isMark? withDescription: ''} placement="right" disableHoverListener={!isMark}><li 
                className={clsx(classes.day, { [classes.selectedDate]: isSelected , [classes.mark]: isMark})} 
                onClick={() => {
                  onClick(date);
                }}
              >
                  <span className={classes.date}>{day}</span>
                  <Hidden smUp><span>{withDescription}</span></Hidden>
                </li>
              </Tooltip>
            }
          })}
        </ul>
    </div>
  );
}

export default Days;