import React, { useState } from 'react';
import { addMonths, subMonths, getMonth, getDaysInMonth, getDay, isSameDay } from 'date-fns'
import { makeStyles, Grid, Hidden } from '@material-ui/core';
import CalendarAppBar from './components/CalendarAppbar'
import { eachDayOfInterval } from 'date-fns/esm';
import Days from './components/Days'
import DescriptionDialog from './components/Dialog';
import DateDescription from './types/DateDescription'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  weekdayContainer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(2),
    backgroundColor: '#eee'
  },
  weekName: {
    flex: 1,
    textAlign: 'center',
    fontSize: 16,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: '#7c7c7c',
  },
}));

const App: React.FC = () => {
  const weekdays: string[] = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
  const [isDialogOpen, setDialogOpen] = useState<boolean>(false);
  const [currentDate, setcurrentDate] = useState<Date>(new Date());
  const [selectedDate, setSelectedDate] = useState<Date>(new Date());
  const [dateWithDescription, setDateWithDescription] = useState<DateDescription[]>([]);
  const [daysInMonth, setDaysInMonth] = useState<Date[]>(
    eachDayOfInterval({
      start: new Date(2020, getMonth(currentDate), 1),
      end: new Date(2020, getMonth(currentDate), getDaysInMonth(currentDate))
    })
  )
  const handleDateChange = (date: Date) => {
    setSelectedDate(date);
    setDialogOpen(true);
  };

  const onDialogSave = (value: string, date: Date) => {
    const filteredArray = dateWithDescription.filter(day => !isSameDay(day.date, date))
    if (value.trim() === '') {
      setDateWithDescription(filteredArray);
    } else {
      setDateWithDescription([...filteredArray, {
        date: date,
        description: value.trim(),
      }])
    }
    setDialogOpen(false);
  };
  
  const onDialogClose = () => {
    setDialogOpen(false);
  };

  const changeMonth = (type: string) => {
    var newMonth: Date;
    if (type === 'next') {
      newMonth = addMonths(currentDate,1);     
    } else {
      newMonth = subMonths(currentDate,1);
    }
    setcurrentDate(newMonth);
    setDaysInMonth(
      eachDayOfInterval({
        start: new Date(2020, getMonth(newMonth), 1),
        end: new Date(2020, getMonth(newMonth), getDaysInMonth(newMonth))
      })
    )
  }

  const hasDescription = (date: Date): string => {
    const index = dateWithDescription.findIndex(day => isSameDay(day.date, date));
    if (index !== -1 ) {
      return dateWithDescription[index].description;
    } else {
      return '';
    }
  }

  const classes = useStyles();
  return (
    <div className={classes.root}>
      {console.log(currentDate,getDay(currentDate))}
      <CalendarAppBar selectedDate = {currentDate} changeMonth = {changeMonth} />
      <Grid item xs={12} className={classes.weekdayContainer}>
        <Grid container>
          {weekdays.map((weekday, index) => ( <div key={index} className={classes.weekName}>{weekday}</div> ))}
        </Grid>
      </Grid>
      <Days daysInMonth={daysInMonth} selectedDate={selectedDate} onClick={handleDateChange} dateWithDescription={dateWithDescription}></Days>
      <DescriptionDialog selectedDate={selectedDate} open={isDialogOpen} description={hasDescription(selectedDate)} onSave={onDialogSave} onClose={onDialogClose}></DescriptionDialog>
      <Hidden smUp><CalendarAppBar selectedDate = {currentDate} changeMonth = {changeMonth}/></Hidden>
    </div>
  );
}

export default App;