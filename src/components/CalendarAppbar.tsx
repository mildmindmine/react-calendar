import React from 'react';
import AppBar from '@material-ui/core/AppBar'
import { format, getMonth } from 'date-fns'
import { Toolbar, Typography, IconButton, makeStyles } from '@material-ui/core';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIos from '@material-ui/icons/ArrowForwardIos';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }));

interface CalendarAppbarProps {
    selectedDate: Date;
    changeMonth: (type: string) => void;
}

const CalendarAppbar: React.FC<CalendarAppbarProps> = props => {
    const {selectedDate, changeMonth} = props;
    const classes = useStyles(props);
    
    return (
        <AppBar position="static">
        <Toolbar>
          {getMonth(selectedDate) !== 0 && 
          <IconButton edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="back"
            onClick={() => changeMonth("prev")}>
            <ArrowBackIos />
          </IconButton>
          }
          <Typography variant="h6" className={classes.title} align='center'>{format(selectedDate, 'MMMM yyyy')}</Typography>
          {getMonth(selectedDate) !== 11 && 
          <IconButton edge="start" 
            className={classes.menuButton} 
            color="inherit" 
            aria-label="menu"
            onClick={() => changeMonth("next")}>
            <ArrowForwardIos />
          </IconButton>
          }
        </Toolbar>
      </AppBar>
    );
}

export default CalendarAppbar;