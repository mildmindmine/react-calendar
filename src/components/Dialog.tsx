import React, { FC, ChangeEventHandler, useState } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { TextField, Button, DialogContentText, DialogActions, DialogContent } from '@material-ui/core';
import { format } from 'date-fns';

interface DescriptionDialogProps {
    selectedDate: Date;
    open: boolean;
    description: string;
    onSave: (value: string, date: Date) => void;
    onClose: () => void;
}

const DescriptionDialog: FC<DescriptionDialogProps> = (props) => {
  const { selectedDate, onSave, onClose, description, open } = props;
  const [txtDescription, setTxtDescription] = useState('');

  const handleSave = () => {
    onSave(txtDescription, selectedDate);
  };
  const handleClose = () => {
    onClose();
  }

  const updateDescription: ChangeEventHandler<HTMLInputElement> = event =>
  setTxtDescription(event.target.value);

  return (
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" TransitionProps={{
        onEnter: () => {
          if(description.trim().length === 0){
            setTxtDescription('');
          }else{
            setTxtDescription(description);
          }
        },
      }}>
          <DialogTitle id="form-dialog-title">{format(selectedDate, 'd MMMM yyyy')}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Add your day's description
            </DialogContentText>
            <TextField
              autoFocus
              multiline
              margin="dense"
              id="description"
              label="Description"
              type="description"
              onChange={updateDescription}
              value={txtDescription}
              inputProps={{maxLength: 40}}
              rows="4"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleSave} color="primary">
              Save
            </Button>
          </DialogActions>
      </Dialog>
  
  );
}

export default DescriptionDialog;

