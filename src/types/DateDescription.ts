interface DateDescription {
    date: Date;
    description: string;
}

export default DateDescription;
